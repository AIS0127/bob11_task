from flask import Flask, request
app = Flask(__name__)



@app.route('/', methods=['GET'])
def home():
    return 'hello bob from user99'

@app.route('/add', methods=['GET'])
def add(a=0,b=0,isserver=0):
    if isserver:
        return a+b
    else:
        return str(int(request.args.get('a'))+int(request.args.get('b')))
   
@app.route('/sub', methods=['GET'])
def sub(a=0,b=0,isserver=0):
    if isserver:
        return a - b
    else:
        return str(int(request.args.get('a'))-int(request.args.get('b')))


if __name__ == '__main__':  
   app.run('0.0.0.0',port=8099,debug=True)
