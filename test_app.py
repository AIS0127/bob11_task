import unittest
import app

class TestApp(unittest.TestCase):
    def test_greet(self):
        result = app.home()
        self.assertEqual(result,'hello bob from user99')
    def test_add(self):
        result = app.add(2,3,1)
        self.assertEqual(result,5)
    def test_sub(self):
        result = app.sub(3,1,1)
        self.assertEqual(result,2)

if __name__=="__main__":
    unittest.main()

